﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.IO;
using Sandbox;
using Sandbox.Screens;
using Sandbox.Helpers;
using Sandbox.GameWorld;


namespace Sandbox
{
    public class SandboxGame : Game
    {
        #region Fields
        public enum GameStateEnum
        {
            SpashScreen,
            MainMenu,
            PlayingGame,
            Inventory
        }

        private static readonly SandboxGame sandboxGame = new SandboxGame();
        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        private GameStateEnum gameState;
        #endregion

        #region PublicMethods
        // Паттерн "Одиночка"
        public static SandboxGame Instance
        {
            get
            {
                return sandboxGame;
            }
        }

        public int ScreenWidth { get; private set; }

        public int ScreenHeight { get; private set; }

        public Camera Camera { get; private set; }

        public World World { get; private set; }
        #endregion

        #region Loading
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            AudioHelper.LoadSounds();
            LoadHelper.Load(Content);

            base.LoadContent();
        }
        #endregion

        #region Update
        protected override void Update(GameTime gameTime)
        {
            // Big switch here
            KeyboardInput();
            MouseInput();
            World.Update();
            base.Update(gameTime);
        }

        private void MouseInput()
        {
           // MouseState mouse = Mouse.GetState();
        }

        private void KeyboardInput()
        {
            KeyboardState keyboard = Keyboard.GetState();
            if (keyboard.IsKeyDown(Keys.Escape))
                gameState = GameStateEnum.MainMenu;
        }
        #endregion

        #region Draw
        protected override void Draw(GameTime gameTime)
        {
            graphics.GraphicsDevice.Clear(Color.Black);
            World.Draw(spriteBatch);
            base.Draw(gameTime);
        }
        #endregion

        #region Methods
        protected override void Initialize()
        {
            base.Initialize();
        }

        private SandboxGame()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            AudioHelper.Initialize(this);

            ScreenWidth = graphics.PreferredBackBufferWidth = Settings.Default.ScreenWidth;
            ScreenHeight = graphics.PreferredBackBufferHeight = Settings.Default.ScreenHeight;

            World = new World(Settings.Default.WorldWidth, Settings.Default.WorldHeight);
            Camera = new Camera(ScreenWidth, ScreenHeight, World.Width, World.Height);
            gameState = GameStateEnum.MainMenu;
        }
        #endregion
    }
}
