using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Sandbox.Helpers;

namespace Sandbox.Screens
{
    class MainMenu
    {
        #region Fields
        // ����� ������ ������� (�� ��������� Play game)
        private int selected = 0;
        private bool arrowPressed = true;
        //private DateTime lastPressTime = DateTime.MinValue;
        #endregion

        #region Update
        public void Update()
        {
            KeyboardState keyboard = Keyboard.GetState();
            if (!arrowPressed)
            {
                if (keyboard.IsKeyDown((Keys.Up)))
                    selected = selected == 0 ? 1 : 0;
                if (keyboard.IsKeyDown((Keys.Down)))
                    selected = selected == 0 ? 1 : 0;
            }
            //if (keyboard.IsKeyDown((Keys.Enter)))
                //game.GameState = selected == 0 ? Sandbox.GameStateEnum.PlayingGame : GameState.Exit;

            if (keyboard.IsKeyUp((Keys.Up)) && keyboard.IsKeyUp((Keys.Down)))
            {
                arrowPressed = false;
            }
            else
            {
                arrowPressed = true;
                AudioHelper.PlaySound("menuClick");
            }
        }
        #endregion

        #region Draw
        public void Draw(SpriteBatch spritebatch)
        {
            spritebatch.Begin();

            spritebatch.DrawString(LoadHelper.Fonts[FontEnum.Arial42], "Sandbox", new Vector2(180, 20), Color.White);

            spritebatch.DrawString(LoadHelper.Fonts[FontEnum.Arial22], "Play game", new Vector2(225, 150), selected == 0 ? Color.LightGreen : Color.DarkGreen);

            spritebatch.DrawString(LoadHelper.Fonts[FontEnum.Arial22], "Exit", new Vector2(270, 220), selected == 1 ? Color.LightGreen : Color.DarkGreen);

            spritebatch.End();
        }
        #endregion
    }
}
