using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Sandbox.Helpers
{
    static class LoadHelper
    {
        public static Dictionary<TextureEnum, Texture2D> Textures;
        public static Dictionary<FontEnum, SpriteFont> Fonts;

        public static void Load(ContentManager content)
        {
            Textures = new Dictionary<TextureEnum, Texture2D>();
            Fonts = new Dictionary<FontEnum, SpriteFont>();

            Textures.Add(TextureEnum.Ground, content.Load<Texture2D>(@"Textures\Environment\Blocks\Ground"));
            
            Fonts.Add(FontEnum.Arial22, content.Load<SpriteFont>(@"Fonts\Arial22"));
            Fonts.Add(FontEnum.Arial42, content.Load<SpriteFont>(@"Fonts\Arial42"));
        }
    }

    public enum TextureEnum
    {
        Ground
    }

    public enum FontEnum
    {
        Arial22,
        Arial42
    }
}
