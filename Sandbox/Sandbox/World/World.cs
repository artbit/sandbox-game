﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;

namespace Sandbox.GameWorld
{
    // В данном классе содержится вся логика игры
    public class World
    {
        #region Fields
        private WorldGenerator worldGenerator;
        #endregion

        #region PublicMethods
        public World(int width, int height)
        {
            Width = width;
            Height = height;
            worldGenerator = new WorldGenerator(width, height);
        }

        public int Width { get; private set; }
        public int Height { get; private set; }
        #endregion

        #region Update
        public void Update()
        {
            
        }
        #endregion

        #region Draw
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();

            spriteBatch.End();

        }
        #endregion
    }
}
