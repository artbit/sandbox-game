using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Sandbox.Helpers;

namespace Sandbox.GameWorld
{
    abstract public class WorldObject
    {
        #region Fields
        protected Vector2 position;
        protected float scale;
        protected float rotation;
        protected Texture2D Texture;
        #endregion

        #region PublicMethods
        public WorldObject(Vector2 position, float scale, Texture2D texture)
        {
            this.position = position;
            this.scale = scale;
            this.Texture = texture;
            this.Width = (int)(Texture.Width * scale);
            this.Height = (int)(Texture.Height * scale);
        }

        public Vector2 Position { get; set; }

        public int Width { get; set; }

        public int Height { get; set; }

        public Rectangle Rectangle
        {
            get
            {
                return new Rectangle((int) position.X, (int) position.Y, Width, Height);
            }
        }

        public void Rotate(float angle)
        {
            rotation += angle;
        }

        public bool Collides(WorldObject other)
        {
            return this.Rectangle.Intersects(other.Rectangle);
        }

        public virtual void Update()
        {
            
        }

        public virtual void Draw(SpriteBatch spritebatch)
        {
            spritebatch.Draw(Texture, Rectangle, Color.White);
            spritebatch.Draw(Texture, Rectangle, null, Color.White, rotation, new Vector2(Texture.Width/2, Texture.Height/2), SpriteEffects.None, 0);
        }
        #endregion
    }
}
