﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Sandbox.Helpers;
using Sandbox.GameWorld;

namespace Sandbox.GameObjects
{
    class Block: WorldObject
    {
        public Block(Vector2 position, float scale, TextureEnum blockTexture) : 
            base(position, scale, LoadHelper.Textures[blockTexture])
        {
        }
    }
}
