﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sandbox.GameWorld
{
    class WorldGenerator
    {
        #region Fields
        private const double AIR_PECENT = 0.2; // 0 < x < 1
        private const double GROUND_PERCENT = 0.25; // 0 < x < 1
        private const double GROUND_ROUGHNESS = 0.6; // 0 < x < 1
        private const double DUNGEON_PROBABILITY = 44; // 0 < x < 100
        private const double GROUND_DUNGEON_PROBABILITY = 33; // 0 < x < 100
        private const int SMOOTH_ITERATIONS = 500000;
        private const int NEIGHBOURS_TO_CLOSE = 4; // 0 < x < 8
        private int width;        
        private int height;
        private int[] heights;
        private int airHeight;
        private int groundHeight;
        private int undergroundHeight;
        private double maxGroundRoughness;
        private Random random;
        #endregion

        #region PublicMethods
        public int[,] Map { get; private set; }

        public WorldGenerator(int height, int width)
        {
            this.height = height;
            this.width = width;

            Map = new int[height, width];
            heights = new int[width];
            airHeight = (int)(height * AIR_PECENT);
            groundHeight = (int)(height * GROUND_PERCENT);
            undergroundHeight = (int)(height - (airHeight + groundHeight));
            random = new Random();
        }

        public void Generate()
        {
            GenerateLandscape();
            GenerateDungeons();
        }
        #endregion

        #region Methods
        private void GenerateLandscape()
        {
            heights[0] = groundHeight / 2;
            heights[width - 1] = groundHeight / 2;

            maxGroundRoughness = 1;
            while (((groundHeight / 2) + width * maxGroundRoughness) > groundHeight)
                maxGroundRoughness -= 0.001;

            DisplaceMidPoint(0, width - 1);

            // Place ground under heights
            for (int i = airHeight; i < airHeight + groundHeight; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (i - airHeight < heights[j])
                        Map[i, j] = 0;
                    else
                        Map[i, j] = 1;
                }
            }
        }

        private void DisplaceMidPoint(int left, int right)
        {
            int length = right - left;
            if (length <= 1)
                return;

            double roughness;
            double maxPossible = (heights[left] + heights[right]) / 2 + length * GROUND_ROUGHNESS;
            double minPossible = (heights[left] + heights[right]) / 2 - length * GROUND_ROUGHNESS;
            if (maxPossible > groundHeight || minPossible < 0)
                roughness = maxGroundRoughness;
            else
                roughness = GROUND_ROUGHNESS;

            int h = (heights[left] + heights[right]) / 2 + random.Next((int)(-1 * length * roughness), (int)(length * roughness));
            int mid = (right + left) / 2;
            heights[mid] = h;

            DisplaceMidPoint(left, mid);
            DisplaceMidPoint(mid, right);
        }

        private void GenerateDungeons()
        {
            for (int i = airHeight; i < airHeight + groundHeight; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (Map[i, j] == 1)
                    {
                        if (random.Next(0, 100) < GROUND_DUNGEON_PROBABILITY)
                            Map[i, j] = 0;
                    }
                }
            }

            for (int i = height - 1; i > airHeight + groundHeight; i--)
            {
                for (int j = 0; j < width; j++)
                {
                    if (random.Next(0, 100) < DUNGEON_PROBABILITY)
                    {
                        Map[i, j] = 0;
                    }
                    else
                    {
                        Map[i, j] = 1;
                    }
                }
            }

            for (int i = 0; i < SMOOTH_ITERATIONS; i++)
            {
                int x = random.Next(0, width - 1);
                int y = random.Next(airHeight, height - 1);

                // Examine all neighbours
                int closedNeighbours = 0;
                for (int dx = -1; dx < 2; dx++)
                {
                    for (int dy = -1; dy < 2; dy++)
                    {
                        int newX = x + dx;
                        int newY = y + dy;
                        bool xOverBound = newX > width || newX < 0;
                        bool yOverBound = newY > height || newY < 0;
                        if (!xOverBound && !yOverBound)
                        {
                            if (Map[newY, newX] == 1)
                                closedNeighbours++;
                        }

                    }
                }

                if (closedNeighbours > 4)
                    Map[y, x] = 1;
                else
                    Map[y, x] = 0;
            }
        }
        #endregion
    }
}