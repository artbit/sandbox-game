﻿using System;

namespace Sandbox
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// Главная точка входа приложения.
        /// </summary>
        static void Main(string[] args)
        {
            SandboxGame sandboxGame = SandboxGame.Instance;
            sandboxGame.Run();
        }
    }
#endif
}

