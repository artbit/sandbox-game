﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Sandbox.GameWorld;

namespace Sandbox
{
    public class Camera
    {
        #region Fields
        private int worldWidth;
        private int worldHeight;
        private Rectangle view;
        #endregion

        #region PublicMethods
        public Camera(int width, int height, int worldWidth, int worldHeight)
        {
            view = new Rectangle(0, 0, width, height);
            this.worldWidth = worldWidth;
            this.worldHeight = worldHeight;
        }

        public Rectangle View { get { return view; } }

        public void Center(WorldObject obj)
        {
            int x = obj.Rectangle.Center.X - view.Width / 2;
            view.X = (int)MathHelper.Clamp(x, 0, worldWidth - view.Width);

            int y = obj.Rectangle.Center.Y - view.Height / 2;
            view.Y = (int)MathHelper.Clamp(y, 0, worldHeight - view.Height);
        }

        // Преобразовывает мировые координаты в экранные
        public Rectangle ToCamera(Rectangle rectangle)
        {
            rectangle.Offset(-view.Left, -view.Top);
            return rectangle;
        }

        public Vector2 ToCamera(Vector2 vector)
        {
            vector.X -= view.Left;
            vector.Y -= view.Right;
            return vector;
        }

        public Vector2 ToCamera(int x, int y)
        {
            return ToCamera(new Vector2((float)x, (float)y));
        }

        public bool InCamera(Rectangle rectangle)
        {
            return view.Intersects(rectangle);
        }
        #endregion
    }
}
